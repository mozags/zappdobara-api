if(this.campaignId){

    dpd.campaign.get({id: this.campaignId}, function(campaignResult, error) {
        this.campaign = campaignResult;
        
        if(campaignResult !== null){
          if(campaignResult.id){
              dpd.campaignuserscans.get({userId: this.userId, campaignId: this.campaignId, active: 1, archive: 0}, function(results, error) {
                  
                  if(results.length >= 0){
                      this.campaign.uservisits = results.length;
                  }
                  else
                  {
                     this.campaign.uservisits = 0;
                  }
              });

              dpd.redeemcampaign.get({campaignId: this.campaignId, userId: this.userId, active: 1, archive: 0}, function(redeemResults, error) {
                  this.campaign.redeem = redeemResults;
              });
          }
        }
    });


    dpd.campaign.get({id: this.campaignId}, function(campaignResult, error) {
        dpd.campaignuserscans.get({userId: this.userId, campaignId: this.campaignId, active: 1, archive: 0, $sort: {scanDateTime: 1}}, function(results, error) {
            if(results.length >= campaignResult.visitsBeforeDiscount){
              dpd.campaignuserscans.get({userId: this.userId, campaignId: this.campaignId, active: 1, archive: 0, $sort: {scanDateTime: 1}, $limit: campaignResult.visitsBeforeDiscount}, function(limitedResults, error) {
                
                
                
                if(limitedResults.length == campaignResult.visitsBeforeDiscount){
                    dpd.redeemcampaign.post({campaignId: this.campaignId, userId: this.userId, scanObjectCollections: limitedResults, campaignOwnerUserId: campaignResult.campaignOwnerUserId, campaignOwnerId: campaignResult.campaignOwnerId, active: 1, archive: 0}, function(redeemPostResult, error) {
                        
                        for(var i=0; i < limitedResults.length; i++){
                            var updateRecord = {}
                            updateRecord.id = limitedResults[i].id
                            updateRecord.active = 0
                            updateRecord.archive = 1
                            dpd.campaignuserscans.put(updateRecord, function(updatedRecord, error) {
                                  
                            });
                        }
                    });
                }
              });
            }
        });
    });
}
