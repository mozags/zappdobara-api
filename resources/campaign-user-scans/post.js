this.scanDateTime = new Date().getTime();
/*  NOTE: At present we check scans and test if we have enough scans to activate the campaign discount.
    Unfortunatly we cannot get the current saved recird unless we call another api call.
*/
var postRequest = this;
/**
* Deployd limits recursive to 2 so we override it by $limitRecursion
**/
dpd.campaignusercampaigns.get({userId: postRequest.userId, $limitRecursion:100}, function(results, error) {
    console.log(results);
    index = -1;
    if(results != null){
      for(var i = 0, len = results.length; i < len; i++) {
          if (results[i].campaignId === this.campaignId) {
              index = i;
              break;
          }
      }
      
      if(index == -1){
        dpd.campaign.get({id: postRequest.campaignId}, function(results, error) {
          
          
          
          dpd.campaignusercampaigns.post({userId: postRequest.userId, campaignId: this.campaignId, campaignOwnerId: results.campaignOwnerId, campaignOwnerUserId: results.campaignOwnerUserId, active: 1}, function(result, error) {
                  console.log(error, 'ERRRRORRR');
                  // Do something
                  
              });

        });
      }
    }
});

//Get the campaign
dpd.campaign.get({id: this.campaignId}, function(campaignResult, error) {
    dpd.campaignuserscans.get({userId: this.userId, campaignId: this.campaignId, active: 1, archive: 0, $sort: {scanDateTime: 1}}, function(results, error) {
        //We increment the result by one so we take in account the current post request
        this.campaignScans = results.length;
        if(results != null && results.length >= campaignResult.visitsBeforeDiscount){
          dpd.campaignuserscans.get({userId: this.userId, campaignId: this.campaignId, active: 1, archive: 0, $sort: {scanDateTime: 1}, $limit: campaignResult.visitsBeforeDiscount}, function(limitedResults, error) {

            if(limitedResults.length >= campaignResult.visitsBeforeDiscount){
                dpd.redeemcampaign.post({campaignId: this.campaignId, userId: this.userId, scanObjectCollections: limitedResults, campaignOwnerUserId: campaignResult.campaignOwnerUserId, campaignOwnerId: campaignResult.campaignOwnerId, active: 1, archive: 0}, function(redeemPostResult, error) {
                    
                    for(var i=0; i < limitedResults.length; i++){
                        var updateRecord = {}
                        updateRecord.id = limitedResults[i].id
                        updateRecord.active = 0
                        updateRecord.archive = 1
                        dpd.campaignuserscans.put(updateRecord, function(updatedRecord, error) {
                            
                            
                            
                        });
                    }
                });
            }
          });
        }
    });
});
