/**
* @description As deployd loops through its results we append
* this.redeemed List of redeemed vouchers
* this.toRedeem List of awaiting redeem vouchers
**/

if(query.dashboardcampaigns) {
	// this.redeemVouchers = []
	// dpd.redeemcampaign.get({campaignId: this.id}, function(redeemResult, error) { 
	// 	if(redeemResult !== null) {
	// 		this.redeemVouchers.push(redeemResult);
	// 	}
	// });
	dpd.redeemcampaign.get({campaignId: this.id, archive: 1, active: 0}, function(redeemedResult, error) { 
		if(redeemedResult !== null) {
			this.redeemed = redeemedResult.length;
		} else {
			this.redeemed = 0;
		}
	});
	
	dpd.redeemcampaign.get({campaignId: this.id, archive: 0, active: 1}, function(toRedeemResult, error) { 
		if(toRedeemResult !== null) {
			this.toRedeem = toRedeemResult.length;
		} else {
			this.toRedeem = 0;
		}
	});
}